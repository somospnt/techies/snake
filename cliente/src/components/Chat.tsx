import React, { useEffect, useState } from 'react';
import { Button, Col, Container, Form, Row, Stack, Table } from 'react-bootstrap';
import { ChatProps, SocketMensaje } from '../types/types';
import socket from './Socket';

const Chat = ({ jugador }: ChatProps) => {
    const [mensaje, setMensaje] = useState<string>('');
    const [mensajes, setMensajes] = useState<Array<SocketMensaje>>([]);

    useEffect(() => {
        socket.on('mensajes', (mensaje: SocketMensaje) => {
            setMensajes([...mensajes, mensaje]);
        });
    }, [mensajes]);

    const submit = (event: any) => {
        event.preventDefault();
        if (mensaje) {
            socket.emit('mensaje', jugador.nombre, mensaje);
            setMensaje('');
        }
    };

    return (
        <>

            <ul id="messages">
                {mensajes.map(
                    (e, i) => (<li key={i}>{`${e.nombre}: ${e.mensaje}`}</li>)
                )}
            </ul>
            <Form onSubmit={submit} className='formChat' >
                <Stack gap={4}>
                    <Form.Group className="mb-3" >
                        <Container>
                            <Row>
                                <Form.Label>Ingrasa Nombre</Form.Label>
                                <Col>
                                    <Form.Control type="text" placeholder="Nombre" value={mensaje}
                                        onChange={e => setMensaje(e.target.value)} />
                                </Col>
                                <Col>
                                    <Button variant="primary" type="submit">
                                        Enviar
                                    </Button>
                                </Col>
                            </Row>
                        </Container>

                    </Form.Group>

                </Stack>
            </Form>
        </>
    );
};

export default Chat;
